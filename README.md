# Zéro-Mobilité

![Fond](assets/images/fond.png "Title image")

## Points requis :

* • une liste de toutes les lignes existantes avec leurs codes couleurs et leurs informations respectives (numéro de ligne / destination / code couleur etc)

* • chaque catégorie de transport doit avoir son icône dédiée (bus / tram etc)

* • le détail de chaque ligne qui contiendra toutes les informations utiles (horaires / arrêts / etc)

* • vous devrez aussi créer une carte interactive qui présentera les informations suivantes :

* • vous devez avoir un système pour enregistrer vos lignes favorites (localstorage suffisant)

* • vous devez avoir un système d’annonces pour informer les usagers des règles sanitaires en vigueur : ce système devra se lancer au chargement de la page en tant que bandeau haut. Il y aura bien entendu la possibilité de le fermer mais il devra se ré-ouvrir au bout de quelques minutes.
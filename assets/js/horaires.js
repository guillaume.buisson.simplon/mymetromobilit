//var
let burgeropen = false;
let accordeontramopen = false;
let myroute = '';
let swap = true;
let swapnumber = 0;
let favouriteup = false;

const myfavoritelist = JSON.parse(localStorage.getItem('favorite'));

let favobject = {};
favobject.favoris = [];

$( document ).ready(function() {
    findlignes();

    if(myfavoritelist.favoris !== null)
    {
        favobject = myfavoritelist;
    }
  });

//menu burger responsive mobile
$('.burger-menu').click( () => {
    if(!burgeropen)
    {
        $('.my-navbar').css('height', '200px');
        $('.my-navbar > a').css('display', 'flex');
        $('.my-navbar > a').css('width', '100%');
        $('.avatar-open').css('top', '100px');
        burgeropen = true;
    }
    else {
        $('.my-navbar').css('height', '100px');
        $('.my-navbar > a').css('display', 'none');
        $('.avatar-open').css('top', '0');
        burgeropen = false;
    }
});

//accordeon
$('.accordeon-header').click( function() {

    $('.ligne-body').children().remove();
    swap = true;
    swapnumber = 0;

    $('.logo-ligne').text('');
    $('.logo-ligne').css('background-color', 'transparent');
    $('.tableau-horaire').css('display', 'none');
    $('.btn-swap').css('display', 'none');
    $('.img-mode').css('display', 'none');
    $('.favourite-img').css('display', 'none');
    $('.tableau-horaire-display').children().remove();
    if($(this).next('.accordeon-body').hasClass('active'))
    {
        $(this).next('.accordeon-body').removeClass('active');
        return;
    }
    $('.accordeon-body').removeClass('active');
    $(this).next('.accordeon-body').addClass('active');
    findlignes();

})

//fetch
const findlignes = () => {
    $.ajax({
        url: 'https://data.metromobilite.fr/api/routers/default/index/routes',
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
    }).done(function(data){
        console.warn('DONE');
        console.log(data);
        findligne(data);
        
        if(!$('#favorite-ligne-body').children().length)
        {
            console.log('ok');
            $('#favorite-text').text("Il n'y a pas encore de favoris");
        }
        else
        {
            $('#favorite-text').text("Les lignes :");
            console.log('pas 0');
        }

    }).fail(function(error){
        console.warn('FAILLLLL');
        console.log(error);
    }).always(function(){
        console.warn('ALWAYS');
        console.log('Terminé !');
    });
}

const findhoraires = (route) => {
    $.ajax({
        url: `https://data.metromobilite.fr/api/ficheHoraires/json?route=${route}&time=${new Date().getTime()}`,
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
    }).done(function(data){
        console.warn('DONE');
        console.log(data);
        horairedisplay(data);
    }).fail(function(error){
        console.warn('FAILLLLL');
        console.log(error);
    }).always(function(){
        console.warn('ALWAYS');
        console.log('Terminé !');
    });
}

const findligne = (myjson) => {

    let mysrc = '';

    for (let i = 0; i < myjson.length; i++) {

        if(myjson[i].mode = "BUS")
        {
            mysrc = 'assets/images/logo/062-bus.svg';
        }
        else if(myjson[i].mode = "TRAM")
        {
            mysrc = 'assets/images/logo/003-tram.svg';
        }
        else if(myjson[i].mode = "RAIL")
        {
            mysrc = 'assets/images/logo/004-train-2.svg';
        }

        $('.ligne-head').each(function() {
            if($(this).attr('value') === myjson[i].type)
            {
                let mydivs = document.createElement('div');
                $(mydivs).text(myjson[i].shortName);
                $(mydivs).attr('id', myjson[i].id);
                $(mydivs).addClass('on-click-lignes');
                $(mydivs).css('background-color', '#' + myjson[i].color);
                $(mydivs).css('color', '#' + myjson[i].textColor);
                $(mydivs).attr('title', myjson[i].longName)
                $('.img-mode').attr('src', mysrc);
                $('.img-mode').attr('title', myjson[i].mode);
                $(this).children('.ligne-body').append(mydivs);
            }
            else if ($(this).attr('value') === 'Favoris')
            {

                if(myfavoritelist !== null)
                {
                    for (let k = 0; k < myfavoritelist.favoris.length; k++) {
                        if(myfavoritelist.favoris[k] === myjson[i].id)
                        {
                            let mydiv = document.createElement('div');
                            $(mydiv).text(myjson[i].shortName);
                            $(mydiv).attr('id', myjson[i].id);
                            $(mydiv).addClass('on-click-lignes');
                            $(mydiv).css('background-color', '#' + myjson[i].color);
                            $(mydiv).css('color', '#' + myjson[i].textColor);
                            $(mydiv).attr('title', myjson[i].longName)
                            $('.img-mode').attr('src', mysrc);
                            $('.img-mode').attr('title', myjson[i].mode);
                            $(this).children('.ligne-body').append(mydiv);
                        }
                    }
                }
            }
          });
    }
}

const horairedisplay = (myjson) => {

    $('.tableau-horaire-display').children().remove();
    for (let i = 0; i < myjson[swapnumber].arrets.length; i++) {

        let myspan = document.createElement('span');
        let myunix = eval(myjson[swapnumber].arrets[i].trips[0]*1000);
        let myDateunix = new Date(myunix);
        let myhours = myDateunix.getHours() < 10 ?'0' + myDateunix.getHours() : myDateunix.getHours();
        let mymin = myDateunix.getMinutes() < 10 ?'0' + myDateunix.getMinutes() : myDateunix.getMinutes();
        let myDate = myhours + 1 + ':' + mymin;
        $(myspan).text('' + myDate);

        let myspan2 = document.createElement('span');
        let myunix2 = eval(myjson[swapnumber].arrets[i].trips[1]*1000);
        let myDateunix2 = new Date(myunix2);
        let myhours2 = myDateunix2.getHours() < 10 ?'0' + myDateunix2.getHours() : myDateunix2.getHours();
        let mymin2 = myDateunix2.getMinutes() < 10 ?'0' + myDateunix2.getMinutes() : myDateunix2.getMinutes();
        let myDate2 = myhours2 + 1 + ':' + mymin2;
        $(myspan2).text('' + myDate2);

        let mytr = document.createElement('tr');
        $(mytr).html(`<td>${myjson[swapnumber].arrets[i].stopName}</td><td>${$(myspan).text()}</td><td>${$(myspan2).text()}</td>`);
        $('.tableau-horaire-display').append(mytr);
    }
}

$(document).on('click', '.on-click-lignes', function() {

    if($(this).attr('id') != null)
    {
        let route = $(this).attr('id');
        $('.logo-ligne').text($(this).text());
        $('.logo-ligne').css('background-color', $(this).css('background-color'));
        $('.logo-ligne').css('color', $(this).css('color'));
        $('.logo-ligne').attr('title', $(this).attr('title'));
        $('.logo-ligne').attr('value', $(this).attr('id'));
        $('.btn-swap').css('display', 'block');
        $('.tableau-horaire').css('display', 'table');
        $('.favorite-ligne-head').css('display', 'none');
        $('.img-mode').css('display', 'block');
        $('.favourite-img').css('display', 'block');

        console.log($(this).parent().hasClass('favorite-stats'));

        if($(this).parent().hasClass('favorite-stats'))
        {
            $('.favorite-ligne-head').css('display', 'flex');
        }

        findhoraires(route);
        myroute = route;
    }

    for (let i = 0; i < favobject.favoris.length; i++) {

        if(favobject.favoris[i] === $('.logo-ligne').attr('value'))
        {
            $('.favourite-img').attr('src', 'assets/images/logo/favouritedone.svg');
            favouriteup = true;
            return;
        }
        else {
            $('.favourite-img').attr('src', 'assets/images/logo/favourite.svg');
            favouriteup = false;
        }
        
    }
})

$(document).on('click', '.btn-swap', function(){

    if(swap)
    {
        swapnumber = 1;
    }
    else {
        swapnumber = 0;
    }
    swap = !swap;
    findhoraires(myroute);
})

$(document).on('click', '.favourite-img', function(){

    const myid = $('.logo-ligne').attr('value');

    if(!favouriteup) {
        favobject.favoris.push(myid);
        $('.favourite-img').attr('src', 'assets/images/logo/favouritedone.svg');
        favouriteup = true;
        localStorage.setItem('favorite', JSON.stringify(favobject));


    }
    else {
        favobject.favoris.splice(favobject.favoris.indexOf(myid), 1);
        $('.favourite-img').attr('src', 'assets/images/logo/favourite.svg');
        favouriteup = false;
        localStorage.setItem('favorite', JSON.stringify(favobject));

        for (let i = 0; i < $('#favorite-ligne-body').children().length; i++) {
            
            if($('#favorite-ligne-body').children()[i].id === myid)
            {
                $('#favorite-ligne-body').children()[i].remove();
                $('.favorite-ligne-head').css('display', 'none');
            }
        }


        if($('#favorite-ligne-body').children().length === 0)
        {
            console.log($('#favorite-ligne-body').children().length);
            $('#favorite-text').text("Il n'y a pas encore de favoris");
        }
        else {
            $('#favorite-text').text("Les lignes :");
        }
    }
})
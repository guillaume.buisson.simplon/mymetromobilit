//ma map
const mymap = L.map('my-map').setView([45.176, 5.73], 13);
//layer
const Stadia_AlidadeSmoothDark = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/dark-v10',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoic29jbG91ZCIsImEiOiJja2VhMmN5ZmUyY20yMzBvYmcyNHZ3amx0In0.dbGhhpR8D8PkbWtnK6OmrA'
});

//var
let burgeropen = false;
let ongletopen = false;
let ligneup = false;

let linecolor = '';

let myidarray = [];

$( document ).ready(function() {
    findlignes();
  });


//fetch
const findlignes = () => {
    $.ajax({
        url: 'https://data.metromobilite.fr/api/routers/default/index/routes',
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
    }).done(function(data){
        console.warn('DONE');
        console.log(data);
        findligne(data);
    }).fail(function(error){
        console.warn('FAILLLLL');
        console.log(error);
    }).always(function(){
        console.warn('ALWAYS');
        console.log('Terminé !');
    });
}

const findgeoloc = (numdeligne) => {
    $.ajax({
        url: `https://data.metromobilite.fr/api/lines/json?types=ligne&codes=${numdeligne}`,
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
    }).done(function(data){
        console.warn('DONE');
        console.log(data);
        writeonmap(data);
    }).fail(function(error){
        console.warn('FAILLLLL');
        console.log(error);
    }).always(function(){
        console.warn('ALWAYS');
        console.log('Terminé !');
    });
}

const findarrest = (numdeligne) => {
    $.ajax({
        url: `https://data.metromobilite.fr/api/ficheHoraires/json?route=${numdeligne}&time=${new Date().getTime()}`,
        type: "GET", // par défaut
        dataType: "json", // ou HTML par ex
    }).done(function(data){
        console.warn('DONE');
        console.log(data);
    }).fail(function(error){
        console.warn('FAILLLLL');
        console.log(error);
    }).always(function(){
        console.warn('ALWAYS');
        console.log('Terminé !');
    });
}

$('.burger-menu').click( () => {
    if(!burgeropen)
    {
        $('.my-navbar').css('height', '200px');
        $('.my-navbar > a').css('display', 'flex');
        $('.my-navbar > a').css('width', '100%');
        $('.ligne-section-trajet').css('margin-top', '200px')
        $('.ligne-section-trajet > img').css('top', '210px')
        $('.avatar-open').css('top', '100px');
        burgeropen = true;
    }
    else {
        $('.my-navbar').css('height', '100px');
        $('.my-navbar > a').css('display', 'none');
        $('.ligne-section-trajet').css('margin-top', '100px')
        $('.ligne-section-trajet > img').css('top', '110px')
        $('.avatar-open').css('top', '0');
        burgeropen = false;
    }
});

const findligne = (myjson) => {

    for (let i = 0; i < myjson.length; i++) {

        let myopacity = 0.5;

            if($('.onglet-actif').attr('value') === myjson[i].type)
            {
                for (let k = 0; k < myidarray.length; k++) {
                    const element = myidarray[k];
                    if(myjson[i].id === element)
                    {
                        myopacity = 1;
                    }
                }
                let mydivs = document.createElement('div');
                $(mydivs).text(myjson[i].shortName);
                $(mydivs).attr('id', myjson[i].id);
                $(mydivs).addClass('on-click-lignes');
                $(mydivs).css('background-color', '#' + myjson[i].color);
                $(mydivs).css('opacity', myopacity);
                $(mydivs).css('color', '#' + myjson[i].textColor);
                $(mydivs).attr('title', myjson[i].longName)

                if($('.onglet-actif').attr('value') === 'Structurantes' || $('.onglet-actif').attr('value') === 'Interurbaines' || $('.onglet-actif').attr('value') === 'C38')
                {
                    $('.infos').children().css('width', '60px');
                    $('.infos').children().css('height', '40px');
                    $('.infos').children().css('border-radius', '0%');
                }

                $('.infos').append(mydivs);
            }
    }
}

$(document).on('click', '.onglets', function(){

    $('.onglets').removeClass('onglet-actif');
    $('.infos').children().remove();
    $(this).addClass('onglet-actif');
    findlignes();

    if($('.ligne-section-trajet > img').css('display') != 'none');
    {
        $('.ligne-section-trajet > img').css('rotate', '-90deg')
        $('.onglets').css('display', 'none');
        ongletopen = !ongletopen;
    }
})

$(document).on('click', '.on-click-lignes', function(){

    let myroute = $(this).attr('id');
        $(this).addClass('ligneup');
        console.log($(this).css('background-color'));
        linecolor = $(this).css('background-color');
        findgeoloc(myroute);
        findarrest(myroute);
        $(this).css('border', '1px solid transparent');

        if($(this).css('opacity') === '0.5')
        {
            $(this).css('opacity', '1');
            myidarray.push($(this).attr('id'));
        }
        else {
            $(this).css('opacity', '0.5');
            delete myidarray[myidarray.indexOf($(this).attr('id'))];
        }
})

$('.ligne-section-trajet > img').click( () => {

    if(!ongletopen)
    {
        $('.ligne-section-trajet > img').css('rotate', '90deg')
        $('.onglets').css('display', 'block');
    }
    else {
        $('.ligne-section-trajet > img').css('rotate', '-90deg')
        $('.onglets').css('display', 'none');
    }

    ongletopen = !ongletopen;
})

const mygeojsonarray = {};
const writeonmap = (myjson) => {

    if(myjson.features[0].geometry.coordinates != null)
    {
        const mycoord = myjson.features[0].geometry.coordinates[0];

    var myLines = [{
        'type': 'LineString',
        'coordinates': mycoord
    }];

    var myStyle = {
        'color': linecolor,
        'weight': 5,
        'opacity': 1
    };

    const geo = L.geoJSON(myLines, {
        style: myStyle
    });

    if (!mygeojsonarray[myjson.features[0].properties.CODE]) {
        mygeojsonarray[myjson.features[0].properties.CODE] = geo;
        mygeojsonarray[myjson.features[0].properties.CODE].addTo(mymap);
    } else {
        mygeojsonarray[myjson.features[0].properties.CODE].removeFrom(mymap);
        mygeojsonarray[myjson.features[0].properties.CODE] = undefined;
    }

    }
    else {
        alert("pas d'infos");
    }
    console.log(mygeojsonarray);
}

Stadia_AlidadeSmoothDark.addTo(mymap);
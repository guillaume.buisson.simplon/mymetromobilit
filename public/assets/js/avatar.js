//var
let avataropen = false;

let basesrc = localStorage.getItem('avatarsrc');

if(basesrc != null)
{
    $('.my-avatar > img').attr('src', basesrc);
}

$(document).on('click', '.my-avatar', function() {
    if(!avataropen)
    {
        $('.avatar-open').css('height', 'auto');
        $('.avatar-open > img').css('display', 'block');
    }
    else {
        $('.avatar-open').css('height', '0px');
        $('.avatar-open > img').css('display', 'none');
    }

    avataropen = !avataropen;
})

$(document).on('click', '.avatar-open > img', function() {

    let mysrc = $(this).attr('src');
    $('.my-avatar > img').attr('src', mysrc);
    localStorage.setItem('avatarsrc', mysrc);
    $('.avatar-open').css('height', '0px');
    $('.avatar-open > img').css('display', 'none');

    avataropen = !avataropen;
});
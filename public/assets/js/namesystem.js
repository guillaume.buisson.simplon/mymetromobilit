let myname = localStorage.getItem('name');

if(myname === null)
{
    $('.name-section').css('display', 'flex');
}
else {
    $('.name-section').css('display', 'none');
    $('.name').text(myname);
}

$('.name-section > input').on('keypress',function(e) {
    if(e.which == 13) {
        if($('.name-section > input').val() != '')
        {
            localStorage.setItem('name', $('.name-section > input').val());
            $('.name').text($('.name-section > input').val());
            $('.name-section').css('display', 'none');
            $('.name-section > input').val('');
        }
    }
});

$(document).on('click', '.name', function() {
    $('.name-section').css('display', 'flex');
})

$(document).on('click', '.ignore-btn', function() {
    $('.name-section').css('display', 'none');
    if($('.name').text() != '')
    {
        $('.name').text($('.name').text());
        localStorage.setItem('name', $('.name').text());
        $('.name-section > input').val('');
    }
    else {
        $('.name').text('Name');
        localStorage.setItem('name', 'Name');
        $('.name-section > input').val('');
    }
})

$(document).on('click', '.valide-btn', function() {
    if($('.name-section > input').val() != '')
    {
        localStorage.setItem('name', $('.name-section > input').val());
        $('.name').text($('.name-section > input').val());
        $('.name-section').css('display', 'none');
        $('.name-section > input').val('');
    }
})
//var
let burgeropen = false;

$('.burger-menu').click( () => {
    if(!burgeropen)
    {
        $('.my-navbar').css('height', '200px');
        $('.my-navbar > a').css('display', 'flex');
        $('.my-navbar > a').css('width', '100%');
        $('.avatar-open').css('top', '100px');
        burgeropen = true;
    }
    else {
        $('.my-navbar').css('height', '100px');
        $('.my-navbar > a').css('display', 'none');
        $('.avatar-open').css('top', '0');
        burgeropen = false;
    }
});

$('.exit-button').click( () => {
    $('.pop-up > div').fadeOut();

    setTimeout( () => {
        $('.pop-up > div').fadeIn();
    }, 300000)
})